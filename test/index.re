open Lodash;

let throttle = () => {
    let f = Throttle.throttle((.a) => Js.log(a), 100);
    let f2 = Throttle.throttle2((.a, b) => Js.log2(a, b), 100);
    let f3 = Throttle.throttle3((.a, b, c) => Js.log3(a, b, c), 100);
    let f4 = Throttle.throttle4((.a, b, c, d) => Js.log4(a, b, c, d), 100);

    Array.make(1000000, 0)
    |> Js.Array.forEach(f);

    Array.make(1000000, 0)
    |> Js.Array.forEachi(f2);

    f3("one", "two", "three");
    f4("one", "two")("three", "four");
};

let shuffle = () => {
    Js.log2("shuffle", Shuffle.shuffle([| 1, 2, 3, 4, 5 |]));
};

let map = () => {
    [| 1, 2, 3 |]
    -> Map.mapArray((*)(2))
    -> Js.log;

    [| ("derp", 20), ("doh", 19) |]
    -> Js.Dict.fromArray
    -> Map.mapDict((*)(2))
    -> Js.log;
};

let property = () => {
    {| { "one": { "two": "three" }} |}
    -> Js.Json.parseExn
    -> Js.Json.decodeObject
    -> Belt.Option.getExn
    -> Map.mapDict(Property.property("one.two"))
    -> Js.log;
};

let get = () => {
    {| { "one": { "two": "three" }} |}
    -> Js.Json.parseExn
    -> Js.Json.decodeObject
    -> Belt.Option.getExn
    -> Get.get("one.two")
    -> Js.log;
};

let uniq = () => {
    [| 4, 7, 5, 4, 4, 3, 6, 5 |]
    -> Uniq.uniq
    -> Js.log;
};

get();
Js.log("---");
map();
Js.log("---");
property();
Js.log("---");
shuffle();
Js.log("---");
uniq();
Js.log("---");
throttle();
