[@bs.module] external throttle:
    (((.'a) => 'b), int) => ((. 'a) => 'b) = "lodash/throttle";
[@bs.module] external throttle2:
    (((.'a, 'b) => 'c), int) => ((. 'a, 'b) => 'c) = "lodash/throttle";
[@bs.module] external throttle3:
    (((.'a, 'b, 'c) => 'd), int) => ((. 'a, 'b, 'c) => 'd) = "lodash/throttle";
[@bs.module] external throttle4:
    (((.'a, 'b, 'c, 'd) => 'e), int) => ((. 'a, 'b, 'c, 'd) => 'e) = "lodash/throttle";

let throttle = (f, i) => {
    let f = throttle(f, i);
    (a) => f(.a);
};

let throttle2 = (f, i) => {
    let f = throttle2(f, i);
    (a, b) => f(.a, b);
};

let throttle3 = (f, i) => {
    let f = throttle3(f, i);
    (a, b, c) => f(.a, b, c);
};

let throttle4 = (f, i) => {
    let f = throttle4(f, i);
    (a, b, c, d) => f(.a, b, c, d);
};
