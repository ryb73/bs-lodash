[@bs.module] external mapArray: array('a) => ('a => 'b) => array('b) = "lodash/map";
[@bs.module] external mapDict: Js.Dict.t('a) => ('a => _) => _ = "lodash/map";
