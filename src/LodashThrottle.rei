let throttle: ((. 'a) => 'b, int) => ('a => 'b);
let throttle2: ((. 'a, 'b) => 'c, int) => (('a, 'b) => 'c);
let throttle3: ((. 'a, 'b, 'c) => 'd, int) => (('a, 'b, 'c) => 'd);
let throttle4: ((. 'a, 'b, 'c, 'd) => 'e, int) => (('a, 'b, 'c, 'd) => 'e);
