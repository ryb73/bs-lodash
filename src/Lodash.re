module Get = { include LodashGet; };
module Map = { include LodashMap; };
module Property = { include LodashProperty; };
module Shuffle = { include LodashShuffle; };
module Throttle = { include LodashThrottle; };
module Uniq = { include LodashUniq; };
